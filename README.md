# DL1 Framework

This is the training framework of the neural network based high level flavour tagger DL1.

## Contents

* [Setup](#setup)
* [Preprocessing](#preprocessing)

<!--## What is this?-->

<!--This is the code required for-->

<!--1. preprocessing to prepare the MC b-tagging samples for training and testing-->

<!--1. the training and testing itself-->



## Setup
Checkout the package :
```
git clone https://gitlab.cern.ch/atlas-flavor-tagging-tools/DL1_framework.git
```

Package requirements:

* TensorFlow (https://www.tensorflow.org)

* Keras [use version 2.2.4]

* matplotlib (http://matplotlib.org)

* numpy

* pandas

* HDF5 (http://www.hdfgroup.org/HDF5)

Useful packages

* hep_ml



You have two possibilities either using the provided Docker image from [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-images) and execute it via (for GPUs)
```
singularity exec --nv docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/ml-gpu/ml-gpu:latest bash
```

If you need to use CPUs, you can use the following image
```
singularity exec --contain docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/ml-cpu-atlas/ml-cpu-atlas:latest bash
```



## Preprocessing
In this step the variables are preprocessed and weights for c and light jets are calculated.
First navigate to
```
cd DL1_framework/Preprocessing
```
To get informations how to run the script type
```
python Preprocessing.py -h
```
The script has two modes: training and test. In the training mode ```(--add_dl1 1)```  also the training weights are calculated and stored in the output file while in the test mode ```(--add_dl1 1) ``` no weights are stored.
The output of the preprocessing script consists of one h5-file file containing the preprocessed variables (including weights and labels) and one json parameter dict file containing all shift, scale and default values.


## Keras Training

```
cd DL1_framework/Training
python BatchTrainModel.py -h
```


## Short description how to get from keras hdf5 model to lwtnn model

 * Use the script [`create_vardict.py`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/DL1_framework/blob/develop_manuel/tools/create_vardict.py) to create the variable dictionary containing shift and scale factors.
 * Use [`conv_models.py`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/DL1_framework/blob/develop_manuel/tools/conv_models.py) to split the hdf5 training file into two separate architecture and weights json files
 * Those 3 output files are then taken as input for the lwtnn script which creates the final lwtnn file
 ```
 kerasfunc2json.py architecture.json weights.h5 variables.json > lwtnn_model.json
 ```


 ## VR retraining steps
 This section will quickly describe how the VR track jets training was done.

 ### Sample preparation

 After the ntuples are produced from the DAODs with the [training-dataset-dumper](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper) the script ['create_hybrid-large_files.py'](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/blob/master/create_hybrid-large_files.py) is used to merge the dumper output. It is done such that one file for each ttbar category (b, c, light) is produced since the light jets dominate and at the end one wants the same amount of jets per category for the training.

This sums up to 8 jobs:
* run-VR_Zprime-even-large.sh
* run-VR_ext-Zprime-even-large.sh
* run-VR_c-jets-even-large.sh
* run-VR_u-jets-even-large.sh
* run-VR_b-jets-even-large.sh
* run-VR_ext-Zprime-odd.sh
* run-VR_Zprime-odd.sh
* run-VR_ttbar-odd.sh

where one job looks like
```
# PFlow new taggers with cuts
SCRIPT=/home/mg294/workspace/b_tagging/dataset-dumper/training-dataset-dumper
FPATH=/work/ws/atlas/mg294-b_tagging/ntuples_2018/VRJets/MC16d
TTBAR=${FPATH}/MC16d_ttbar/user.mguth.18347049._0*.h5
ZPRIME=${FPATH}/MC16d_Zprime/user.mguth.18347050._0*.h5
python ${SCRIPT}/create_hybrid-large_files.py --n_split 3 --even --bjets -Z ${ZPRIME} -t ${TTBAR} -n 20000000 -c 1.0 -p 250000 -o ${FPATH}/hybrids/MC16d_hybrid_even_100_VRtrackJets-pTcuts-bjets-stats.h5
```
The output will then be several files depending on the specified `--n_split` option.
Those files need to be merged. (The validation/testing files are not merged since they can then directly used as two sets of samples for validation and testing). The merging can be done via those scrips [hdf5_manipulator](https://gitlab.cern.ch/mguth/hdf5_manipulator?nav_source=navbar).


The next step is to apply the downsampling to the training sample which is done via the [`downsampling.py`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/DL1_framework/blob/master/Preprocessing/downsampling.py) script.

The synthax is the following
```
python downsampling.py -c <json-confg-file>
```
where th config file looks like
```
{
  "file_path": "/work/ws/atlas/mg294-b_tagging/ntuples_2018/VRJets/MC16d/hybrids",
  "Njets": 5e6,
  "ttbar_frac": 0.55,
  "iterations": 6,
  "pTcut": 1.25e5,
  "bhad_pTcut": 2.5e5,
  "pT_max": 3e6,
  "f_z": "MC16d_hybrid_even_0_ext-Zprime_VRtrackJets-pTcuts-stats-file_merged.h5",
  "f_ttb_bjets": "MC16d_hybrid_even_100_VRtrackJets-pTcuts-bjets-stats-file_merged.h5",
  "f_ttb_cjets": "MC16d_hybrid_even_100_VRtrackJets-pTcuts-cjets-stats-file_merged.h5",
  "f_ttb_ujets": "MC16d_hybrid_even_100_VRtrackJets-pTcuts-ujets-stats-file_merged.h5",
  "outfile_name": "/work/ws/nemo/fr_mg1150-DL1-0/VR_samples/downsampled_ext-hybrid-VR_70-train-3TeV_pT.h5",
  "plot_name": "VR_hybrid-3TeV_pT"
}
```
Depending on the value of the "iterations" option N files will be created.

Afterwards the scaling and shifting values need to be retrieved this is done via
```
# VR track jets ext hybrid
INPUTFILE=/work/ws/nemo/fr_mg1150-DL1-0/VR_samples/downsampled_ext-hybrid-VR_70-train-file1_6.h5
OUTDIR=/work/ws/nemo/fr_mg1150-DL1-0/VR_samples/
OUTFILE=DL1Training-VRjets-hybrid_MC16D_2018_${TTFRAC}_preprocessed.h5
python Preprocessing.py --no_writing --downsampled --only_scale --dummy_weights --input_file ${INPUTFILE} -f params_MC16D-2019-VRjets -o ""
```
Note: it is only done for one of the downsampled files and later applied to all of them.

Next, the scaling and shifting factors are applied to all downsampled files
```
for counter in `seq 1 6`;
do
INPUT=/work/ws/nemo/fr_mg1150-DL1-0/VR_samples/downsampled_ext-hybrid-VR_70-train-file${counter}_6.h5
DICTFILE=/home/fr/fr_fr/fr_mg1150/workspace/DL1_framework/Preprocessing/dicts/params_MC16D-2019-VRjets.json
OUTPUTFILE=/work/ws/nemo/fr_mg1150-DL1-0/VR_samples/DL1Training-VRjets_extended-hybrid_MC16D_2019-21M_preprocessed-DL1-stats_file-${counter}.h5
# OUTPUTFILE=/work/ws/nemo/fr_mg1150-DL1-0/VR_samples/DL1rTraining-VRjets_extended-hybrid_MC16D_2019-21M_preprocessed-DL1-stats_file-${counter}.h5
# VARFILE=../Training/configs/DL1rmu-NN_Variables.json
# VARFILE=../Training/configs/DL1r_Variables.json
VARFILE=../Training/configs/DL1_Variables.json
python Apply_Scales.py  --large_file -i ${INPUT} -f ${DICTFILE} -o ${OUTPUTFILE} -v ${VARFILE}
done
```

it has to be done for every variable combination.

The outputfiles need to be merged and are ready to be trained with.


### Training
