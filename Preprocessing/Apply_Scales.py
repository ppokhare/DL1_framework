"""
 ******************************************************************************
 * Project: DL1_framework                                                     *
 * Package: atlas-flavor-tagging-tools                                        *
 *                                                                            *
 * Description:                                                               *
 *      Preprocessing and weight computation script for DL1 tagger            *
 *                                                                            *
 * Authors:                                                                   *
 *      ATLAS flavour tagging group, CERN, Geneva                             *
 *                                                                            *
 ******************************************************************************
"""
from __future__ import print_function
import h5py
import numpy as np
import json
import pandas as pd
from DL1_Variables_preprocess import var_list
import tools_preprocess as tp
import sys
sys.path.append("../Training/.")
from train_tools import Get_TrainingSample


def Apply_Scale(args):
    """Applies the scaling and shifting to a given input sample"""
    # retrieves the argparse options from tools_preprocess.py
    infile_all = h5py.File(args.input_file, 'r')
    print(args.input_file)
    columns = pd.DataFrame(infile_all['bjets'][:1]).columns.values
    label_var = "HadronConeExclTruthLabelID"
    if 'label' in columns:
        label_var = 'label'

    var_list_mod = var_list
    if 'weight' in columns:
        var_list_mod += ['weight']
    if label_var in columns:
        var_list_mod += [label_var]
    if 'category' in columns:
        var_list_mod += ['category']

    del var_list_mod[0]
    with open(args.dict_file, 'r') as infile:
        scale_dict = json.load(infile)

    key_list = ['bjets', 'cjets', 'ujets']
    jets = {}
    outfile_name = '%s' % (args.output_file)
    h5f = h5py.File(outfile_name, 'w')

    for key in key_list:
        X = pd.DataFrame(infile_all[key][:][var_list_mod])
        # X = pd.read_hdf(args.input_file, key, columns=[var_list_mod])
        X.replace([np.inf, -np.inf], np.nan, inplace=True)

        # Replace NaN values with default values from default dictionary
        default_dict = tp.Gen_default_dict(scale_dict)
        X.fillna(default_dict, inplace=True)
        if 'weight' not in columns:
            X['weight'] = np.ones(len(X))
        # scale and shift distribution
        for elem in scale_dict:
            if 'isDefaults' in elem['name']:
                continue
            else:
                X[elem['name']] = (X[elem['name']] - elem['shift'])\
                    / elem['scale']
        if args.info:
            jets[key] = X
        # save output file to disk
        print("Save file as ", outfile_name)
        h5f.create_dataset('train_processed_%s' % key,
                           data=X.to_records(index=False))
        del X
    h5f.close()
    if args.info:
        # Plot all preprocessed variable distributions
        print("Plot all preprocessed variable distributions:", args.plot_name)
        tp.Plot_vars(jets['bjets'], jets['cjets'], jets['ujets'],
                     args.plot_name)


def PrepareLargeTraining(args):
    """Function to prepare large training samples for Data Generator which
    reads each batch from file.
    The input should be already downsampled -- weights are not (yet) supported
    """
    # scaling and shifting the input and writing it to file to save memory
    if args.preprocessed:
        input_file = args.input_file
    else:
        Apply_Scale(args)
        input_file = args.output_file
    print("Using variable config", args.var_file)
    X_train, Y_train = Get_TrainingSample(input_file, do_weights=False,
                                          variable_config=args.var_file)

    h5f = h5py.File(args.output_file, 'w')
    print("Writing out", args.output_file)
    h5f.create_dataset('X_train', data=X_train)
    h5f.create_dataset('Y_train', data=Y_train)

    # infile_all = h5py.File(args.output_file, 'a')

    h5f.close()

    # infile_all.close()


if __name__ == '__main__':
    args = tp.GetParser()
    if args.large_file:
        PrepareLargeTraining(args)
    else:
        Apply_Scale(args)
