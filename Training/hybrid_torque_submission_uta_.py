import subprocess as sp
import os.path
import numpy as np


gpu_queues = ["gpu9thecount",
              "gpu8thecount",
              "gpu3thingtwo",
              "gpu0super",
              "gpu1thingtwo",
              "gpu3thingone",
              "gpu3thingtwo",
              "gpu2thingtwo",
              "gpu4thecount",
              "gpu5thecount",
              "gpu6thecount",
              "gpu7thecount",
              "gpu0thingtwo",
              "gpu0thingone",
              "gpu1thingone"]


def slurm_sh_keras_training(ttbarcomp, q_count):
    """Generates .sh file for torque job submission."""
    queue = gpu_queues[q_count]
    # queue = "gpu_queue"
    N_gpus = 2
    # queue = gpu_queues[1][0]
    # N_gpus= gpu_queues[1][1]
    #transform relative path to absolute path
    abspath = os.path.abspath(".")
    sh_path = abspath+"/"+"run/"
    sh_name = "%skeras_train_results_%i.sh"%(sh_path, ttbarcomp * 100)

    #check if file already exists, if it does exit since .sh should delete itself after running
    if os.path.exists(sh_name):
        print ("File already exists, job might have failed!" )
        return 0
    sh_file = open(sh_name, 'w')
    sh_file.write("#!/bin/sh\n\n")

    sh_file.write("# jobname\n")
    sh_file.write("#PBS -N hybrid_%i\n\n" % (ttbarcomp * 100))

    sh_file.write("# user\n")
    sh_file.write("#PBS -u mg294\n")

    sh_file.write("# define std and error output files\n")
    sh_file.write("#PBS -k oe\n")
    # sh_file.write("#PBS -o %s \n" % ("/scratch/mg294/DL1_framework/Training/run/hybrid_%i.o"% (ttbarcomp * 100)))
    # sh_file.write("#PBS -e %s\n\n" % ("/scratch/mg294/DL1_framework/Training/run/hybrid_%i.e" % (ttbarcomp * 100)))

    sh_file.write("# mail alert at start, end and abortion of execution\n")
    sh_file.write("#PBS -m bea\n\n")

    sh_file.write("# send mail to this address\n")
    sh_file.write("#PBS -M manuel.guth@cern.ch\n\n")

    sh_file.write("# use submission environment\n")
    sh_file.write("#PBS -V\n\n")

    sh_file.write("# set max wallclock time\n")
    sh_file.write("#PBS -l walltime=450:00:0\n\n")

    sh_file.write("# set the number of nodes and processes per node\n")
    sh_file.write("#PBS -l mem=16000mb\n\n")
    # sh_file.write("#PBS -l nodes=1:gpus=%i\n\n" % N_gpus)
    #
    sh_file.write("# set the name of the queue\n")
    sh_file.write("#PBS -q %s\n\n" % queue)



    sh_file.write("# Set up the evironmen\n")
    sh_file.write("export WORKON_HOME=/home/mg294/Envs\n")
    sh_file.write("bash /usr/local/bin/virtualenvwrapper.sh\n")
    sh_file.write(". /home/mg294/Envs/dl1_keras2/bin/activate\n")
    sh_file.write("export PATH=/usr/local/cuda-9.0/bin:${PATH}\n")
    sh_file.write("export LD_LIBRARY_PATH=/usr/local/cuda-9.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}\n")


    # sh_file.write("# start job from the directory it was submitted\n")
    # sh_file.write("cd $PBS_O_WORKDIR\n\n")

    # Submit job
    sh_file.write("#Submit Job \n")
    script_path = '/scratch/mg294/keras2_DL1/DL1_framework/Training/'
    file_path = '/data/mg294/new_ntuples/preprocessed'
    file_path = '/data/mg294/temp_tuples'
 # train_file = '%s/hybrid_training_preprocessed_%i.h5' % (file_path, ttbarcomp * 100)
    train_file = 'hybrid_training_sample_MC16D_reduced_even_renamed_preprocessed_ordered.h5'
    # weight_file = '%s/WeightFile_%i.h5' % (file_path, ttbarcomp * 100)
    #run_script = '%s --t %s --w %s  -m %s'%('BatchTrainModel.py',train_file, weight_file, 'hybrid_%i' % (ttbarcomp * 100))
    run_script = '%s --t %s/%s  -m %s' % ('BatchTrainModel.py', file_path, train_file, 'keras2_model')
    sh_file.write("# start job from file directory\n")
    sh_file.write("cd %s\n\n" % script_path)

    sh_file.write("%s %s%s \n\n" %('python', script_path, run_script))





    #delete the .sh file at the end
    sh_file.write("#delete the .sh file at the end\n")
    sh_file.write("rm %s\n" %(sh_name))


    submit_name = "qsub  %s" % (sh_name)
    # submit_name = "qsub -q gpu_queue  %s" % (sh_name)
    # submit_name = "sbatch -o %s -p medium -t %s -n 1 --cpus-per-task 2 --constraint='DALCO' --mem=%s -D %s %s" %(sh_path+"keras_training_results_hybrid_test_%i.log" % (ttbarcomp * 100),run_time, memory, workdir, sh_name)
    return submit_name






def subm_train_slurm():
    """
        Calls the job generation scripts and submits the jobs to slurm witg the coresponding .sh file
    """

    # Creating .sh files and launching the jobs
    qcount = 0
    l_list = [0.50, 0.55, 0.60, 0.65, 1.0]
    l_list = [0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
    for x in l_list:
    # for x in np.linspace(0.5, 1.0, num=11):
        # if x>0.7 and x!=1.0: continue
        slurm_sub = slurm_sh_keras_training(ttbarcomp=x, q_count=qcount)
        qcount += 1
        #checking if the job already exists or failed
        if slurm_sub == 0:
            continue
        # Launching the jobs
        else:
            test = sp.Popen(slurm_sub, shell=True)
            test.wait()
            # print (slurm_sub)
#subm_train_slurm()





slurm_sub = slurm_sh_keras_training(ttbarcomp=0, q_count=2)
        #checking if the job already exists or failed
if slurm_sub == 0:
	exit()
# Launching the jobs
else:
	test = sp.Popen(slurm_sub, shell=True)
	test.wait()
