"""
 ******************************************************************************
 * Project: DL1_framework                                                     *
 * Package: atlas-flavor-tagging-tools                                        *
 *                                                                            *
 * Description:                                                               *
 *      Training script for DL1 tagger                                        *
 *                                                                            *
 * Authors:                                                                   *
 *      ATLAS flavour tagging group, CERN, Geneva                             *
 *                                                                            *
 ******************************************************************************
"""
import json
import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
import tensorflow as tf
import argparse
import train_tools as tt
import matplotlib.pyplot as plt
import h5py


def GetParser():
    """Argparse option for Preprocessing script."""
    parser = argparse.ArgumentParser(description="""Options for DL1
                                     training""")

    parser.add_argument('-c', '--configs', type=str, required=True)
    parser.add_argument('-e', '--epochs', default=300, type=int, help="Number\
                        of trainng epochs.")
    parser.add_argument('-f', '--file_path', default=None, type=str,
                        help="""Path to training file (from config). If not
                        specified, string from config file used without extra
                        path.""")
    parser.add_argument('--dict_path', default=None, type=str, help="""Path to
                        scale_dict file (from config). If not specified, string
                        from config file used without extra path.""")
    parser.add_argument('--var_dict_path', default=None, type=str,
                        help="""Path to variable_config file (from config).
                        If not specified, string from config file used without
                        extra path.""")
    parser.add_argument('-o', '--out_path', default=None, type=str,
                        help="""Path to output folder. If not specified, new
                        folder with model name created in submission
                        directory.""")

    parser.add_argument('-g', '--gpu_n', default=0, type=int,
                        help="""Determines which GPU should be used.""")
    parser.add_argument('--use_gpu', action='store_true', help='''Option to use
                       GPUs.''')
    parser.add_argument('--large_file', action='store_true', help='''Option to
                        large input files where each batch is read from
                        file.''')
    parser.add_argument('--vr_overlap', action='store_true', help='''Option to
                        enable vr overlap removall for validation sets.''')

    args = parser.parse_args()
    with open(args.configs) as conf:
        args.configs = json.load(conf)
    if args.file_path is None:
        args.train_file = os.path.abspath(args.configs["train_file"])
        args.validation_file = os.path.abspath(args.configs["validation_file"])
        if "add_validation_file" in args.configs:
            args.add_validation_file = os.path.abspath(
                args.configs["add_validation_file"])
        else:
            args.add_validation_file = None

    else:
        args.train_file = os.path.join(args.file_path,
                                       args.configs["train_file"])
        args.validation_file = os.path.join(args.file_path,
                                            args.configs["validation_file"])
        if "add_validation_file" in args.configs:
            args.add_validation_file = os.path.join(args.file_path,
                                                    args.configs[
                                                        "add_validation_file"])
        else:
            args.add_validation_file = None
    # this is just a temporary implementation for a train test split
    # for now the file has to be split already before and should contain
    # besides X_train, Y_train also X_valid, Y_valid
    # and also only available for large file training for now
    # TODO: make this more sophisticated
    if "split_train_valid" in args.configs:
        args.split_train_valid = args.configs["split_train_valid"]
    else:
        args.split_train_valid = None

    if args.dict_path is None:
        args.scale_dict = os.path.abspath(args.configs["scale_dict"])
    else:
        args.scale_dict = os.path.join(args.dict_path,
                                       args.configs["scale_dict"])
    if args.var_dict_path is None:
        args.variable_config = os.path.abspath(args.configs["variable_config"])
    else:
        args.variable_config = os.path.join(args.var_dict_path,
                                            args.configs["variable_config"])
    if args.out_path is None:
        args.model_name = os.path.abspath(args.configs["model_name"])
    else:
        args.model_name = os.path.join(args.out_path,
                                       args.configs["model_name"])
    return args


args = GetParser()
use_gpu = args.use_gpu


import subprocess as sp
import os


def mask_unused_gpus(leave_unmasked=1):
    ACCEPTABLE_AVAILABLE_MEMORY = 1024
    COMMAND = "nvidia-smi --query-gpu=memory.free --format=csv"

    try:
        _output_to_list = lambda x: x.decode('ascii').split('\n')[:-1]
        memory_free_info = _output_to_list(sp.check_output(
            COMMAND.split()))[1:]
        memory_free_values = [int(x.split()[0]) for i, x in
                              enumerate(memory_free_info)]
        available_gpus = [i for i, x in enumerate(memory_free_values)
                          if x > ACCEPTABLE_AVAILABLE_MEMORY]

        if len(available_gpus) < leave_unmasked:
            raise ValueError('Found only %d usable GPUs in the system' %
                             len(available_gpus))
        print("Found %i GPUs with sufficient memory" % len(available_gpus))
        print("will choose number %i in the list which corresponds to GPU %i" %
              (args.gpu_n, available_gpus[args.gpu_n]))
        gpu_cuda = ','.join(map(str, [available_gpus[args.gpu_n]]))
        print(gpu_cuda)
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu_cuda
    except Exception as e:
        print('"nvidia-smi" is probably not installed. GPUs are not masked', e)


if args.use_gpu:
    mask_unused_gpus()


from keras.layers import BatchNormalization
from keras.layers import Dense, Activation, Input, Dropout
from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import ReduceLROnPlateau
import multiprocessing
manager = multiprocessing.Manager()
queue = manager.Queue(maxsize=10)


def NN_model(input_shape):
    NN_config = args.configs["NN_structure"]
    inputs = Input(shape=input_shape)
    x = inputs
    for i, unit in enumerate(NN_config["units"]):
        x = Dense(units=unit, activation="linear",
                  kernel_initializer='glorot_uniform')(x)
        x = BatchNormalization()(x)
        x = Activation(NN_config['activations'][i])(x)
        if "dropout_rate" in NN_config:
            x = Dropout(NN_config["dropout_rate"][i])(x)
    predictions = Dense(units=3, activation='softmax',
                        kernel_initializer='glorot_uniform')(x)

    model = Model(inputs=inputs, outputs=predictions)
    model.summary()

    model_optimizer = Adam(lr=NN_config["lr"])
    model.compile(
        loss='categorical_crossentropy',
        optimizer=model_optimizer,
        metrics=['accuracy']
    )
    return model, NN_config["batch_size"]


def ControlPlots(dictfile):
    """Create simple control plots"""
    with open(dictfile) as dicfile:
        dict_list = json.load(dicfile)
    epochs = []
    loss = []
    val_loss = []
    acc = []
    val_acc = []
    for elem in dict_list:
        epochs.append(elem['epoch'])
        loss.append(elem['loss'])
        val_loss.append(elem['val_loss'])
        acc.append(elem['acc'])
        val_acc.append(elem['val_acc'])
    plt.plot(epochs, loss, label='training loss')
    plt.plot(epochs, val_loss, label='validation loss')
    plt.legend()
    plt.xlabel('Epoch')
    plt.ylabel('loss')
    plt.savefig("%s-ControlPlot.pdf" % args.model_name,
                transparent=True)


def train():
    print("load data")
    X_train, Y_train = tt.Get_TrainingSample(args.train_file,
                                             variable_config=
                                             args.variable_config,
                                             do_weights=False)
    X_valid, Y_valid = tt.Get_TestSamples(args.validation_file,
                                          args.scale_dict,
                                          args.variable_config, origin=True,
                                          vr_overlap=args.vr_overlap)
    if args.add_validation_file is not None:
        X_valid_add, Y_valid_add = tt.Get_TestSamples(args.add_validation_file,
                                                      args.scale_dict,
                                                      args.variable_config,
                                                      origin=True,
                                                      vr_overlap=args.vr_overlap)
        assert X_train.shape[1] == X_valid_add.shape[1]
    else:
        X_valid_add = None
        Y_valid_add = None

    assert X_train.shape[1] == X_valid.shape[1]

    model, batch_size = NN_model((X_train.shape[1],))
    reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.2, patience=5,
                                  min_lr=0.00001)
    my_callback = tt.MyCallback(model_name=args.model_name, X_valid=X_valid,
                                Y_valid=Y_valid, X_valid_add=X_valid_add,
                                Y_valid_add=Y_valid_add)
    callbacks = [reduce_lr, my_callback]
    params = {'batch_size': batch_size,
              'X': X_train,
              'Y': Y_train,
              # 'sample_weight': weights
              }

    training_generator = tt.DataGenerator(**params)
    model.fit_generator(generator=training_generator,
                        validation_data=[X_valid, Y_valid],
                        epochs=args.epochs, callbacks=callbacks,
                        use_multiprocessing=True, workers=6)


def trainLargeFile():
    print("load validation data (training data will be loaded per batch)")
    X_valid, Y_valid = tt.Get_TestSamples(args.validation_file,
                                          args.scale_dict,
                                          args.variable_config, origin=True,
                                          vr_overlap=args.vr_overlap)
    if args.add_validation_file is not None:
        X_valid_add, Y_valid_add = tt.Get_TestSamples(args.add_validation_file,
                                                      args.scale_dict,
                                                      args.variable_config,
                                                      origin=True,
                                                      vr_overlap=args.vr_overlap)
        assert X_valid.shape[1] == X_valid_add.shape[1]
    elif args.split_train_valid is True and args.split_train_valid is not None:
        X_valid_add = h5py.File(args.train_file, "r")["X_valid"][:]
        Y_valid_add = h5py.File(args.train_file, "r")["Y_valid"][:]
        if args.add_validation_file is not None:
            print("WARNING: will not take the additional validation file!",
                  "(split_train_valid option was set to true)")
    else:
        X_valid_add = None
        Y_valid_add = None
    model, batch_size = NN_model((X_valid.shape[1],))
    reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.2, patience=5,
                                  min_lr=0.00001)
    my_callback = tt.MyCallback(model_name=args.model_name, X_valid=X_valid,
                                Y_valid=Y_valid, X_valid_add=X_valid_add,
                                Y_valid_add=Y_valid_add)
    callbacks = [reduce_lr, my_callback]
    file = h5py.File(args.train_file, 'r')
    X_train = file['X_train']
    Y_train = file['Y_train']
    params = {'batch_size': batch_size,
              'X': X_train,
              'Y': Y_train
              }

    training_generator = tt.DataGenerator_ReadFile(**params)
    model.fit_generator(generator=training_generator,
                        validation_data=[X_valid, Y_valid],
                        epochs=args.epochs, callbacks=callbacks,
                        use_multiprocessing=True)

    print("Models saved:", args.model_name)


if __name__ == '__main__':
    if args.large_file:
        trainLargeFile()
    else:
        train()
